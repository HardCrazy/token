<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 06-07-17
 * Time: 10:00
 */

namespace App\APIBundle\Controller;


use App\CoreBundle\Entity\AuthToken;
use App\CoreBundle\Entity\Credentials;
use App\CoreBundle\Entity\User;
use App\CoreBundle\Form\CredentialsType;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;

class AuthTokenController extends FOSRestController
{
    /**
     * @param Request $request
     * @return View|\Symfony\Component\Form\Form|AuthToken
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post(path="/auth-tokens")
     */
    public function postAuthTokensAction(Request $request)
    {
        $credentials = new Credentials();

        $form = $this->createForm(CredentialsType::class, $credentials);
        $form->submit($request->request->all());

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppCoreBundle:User')->findOneBy(array('email' => $credentials->getLogin()));

        if ($user === null) {
            return $this->invalidCredentials();
        }

        $encoder = $this->get('security.password_encoder');
        $isPasswordValid = $encoder->isPasswordValid($user, $credentials->getPassword());

        if (!$isPasswordValid) {
            return $this->invalidCredentials();
        }

        $generator = $this->get('fos_user.util.token_generator');

        $token = new AuthToken();
        $token->setAuthTokenValue($generator->generateToken());
//        $token->setAuthTokenValue(base64_encode(random_bytes(50)));
        $token->setAuthTokenCreatedAt(new \DateTime()); // PrePersist better
        $token->setUser($user);

        $em->persist($token);
        $em->flush();

        return $token;
    }

    /**
     * @param Request $request
     *
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete(path="/auth-tokens/{authTokenID}", requirements={"authTokenID" : "\d*"})
     */
    public function deleteAuthTokensAction(Request $request)
    {
        $em  = $this->getDoctrine()->getManager();
        $authToken = $em->getRepository('AppCoreBundle:AuthToken')->find($request->get('authTokenID'));

        if ($authToken === null) {
            return;
        }

        /** @var User $user */
        $user = $this->getUser();

        if ($user->getId() === $authToken->getUser()->getId()) {
            $em->remove($authToken);
            $em->flush();
        }
    }

    /**
     * @return View
     */
    private function invalidCredentials()
    {
        return View::create(array('message' => 'Bad credentials'), Response::HTTP_BAD_REQUEST);
    }
}