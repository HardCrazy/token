<?php

/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 05-07-17
 * Time: 10:34
 */

namespace App\APIBundle\Controller;

use App\CoreBundle\Entity\Game;
use App\CoreBundle\Form\GameType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GameController extends FOSRestController
{
    /**
     * @param Request $request
     * @return \App\CoreBundle\Entity\Game[]|array
     *
     * @Rest\View()
     * @Rest\Get(path="/games")
     */
    public function getGamesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $games = $em->getRepository('AppCoreBundle:Game')->findAll();

        return $games;
    }

    /**
     * @param Request $request
     * @return Game|View|Form
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post(path="/games")
     */
    public function postGamesAction(Request $request)
    {
        return $this->handleGameData($request);
    }

    /**
     * @param Request $request
     * @return Game|View|Form
     *
     * @Rest\View()
     * @Rest\Put(path="/games/{gameID}", requirements={"gameID" : "\d*"})
     */
    public function putGamesAction(Request $request)
    {
        return $this->handleGameData($request, false);
    }

    /**
     * @param Request $request
     * @return Game|View|Form
     *
     * @Rest\View()
     * @Rest\Patch(path="/games/{gameID}", requirements={"gameID" : "\d*"})
     */
    public function patchGamesAction(Request $request)
    {
        return $this->handleGameData($request, false, false);
    }

    /**
     * @param Request $request
     *
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete(path="/games/{gameID}", requirements={"gameID" : "\d*"})
     */
    public function deleteGamesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $game = $em->getRepository('AppCoreBundle:Game')->find($request->get('gameID'));

        if ($game !== null) {
            $em->remove($game);
            $em->flush();
        }
    }

    /**
     * @param Request $request
     * @param bool $add
     * @param bool $clearMissing
     * @return Game|View|Form
     */
    private function handleGameData(Request $request, $add = true, $clearMissing = true)
    {
        $em = $this->getDoctrine()->getManager();

        if ($add) {
            $game = new Game();
        } else {
            $game = $em->getRepository('AppCoreBundle:Game')->find($request->get('gameID'));

            if ($game === null) {
                return View::create(array('message' => 'The game does not exists !'), Response::HTTP_NOT_FOUND);
            }
        }

        $data = $request->request->all();
        $data['file'] = $request->files->get('file');

        $form = $this->createForm(GameType::class, $game);
        $form->submit($data, $clearMissing);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($add) {
                $em->persist($game);
            }

            $em->flush();
            return $game;
        } else {
            return $form;
        }
    }

}