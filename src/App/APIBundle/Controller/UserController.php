<?php

/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 05-07-17
 * Time: 10:34
 */

namespace App\APIBundle\Controller;

use App\CoreBundle\Entity\User;
use App\CoreBundle\Form\UserType;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends FOSRestController
{
    /**
     * @param Request $request
     * @return \App\CoreBundle\Entity\User[]|array
     *
     * @Rest\View()
     * @Rest\Get(path="/users")
     */
    public function getUsersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppCoreBundle:User')->findAllWithAddress();

        return $users;
    }

    /**
     * @param Request $request
     * @return User
     *
     * @Rest\View()
     * @Rest\Get(path="/users/profile")
     */
    public function getProfileAction(Request $request)
    {
        $user = $this->getUser();

        return $user;
    }

    /**
     * @param Request $request
     * @return User|View|Form
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post(path="/users")
     */
    public function postUsersAction(Request $request)
    {
        return $this->handleUserData($request);
    }

    /**
     * @param Request $request
     * @return User|View|Form
     *
     * @Rest\View()
     * @Rest\Put(path="/users/{userID}", requirements={"userID" : "\d*"})
     */
    public function putUsersAction(Request $request)
    {
        return $this->handleUserData($request, false);
    }

    /**
     * @param Request $request
     * @return User|View|Form
     *
     * @Rest\View()
     * @Rest\Patch(path="/users/{userID}", requirements={"userID" : "\d*"})
     */
    public function patchUsersAction(Request $request)
    {
        return $this->handleUserData($request, false, false);
    }

    /**
     * @param Request $request
     *
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete(path="/users/{userID}", requirements={"userID" : "\d*"})
     */
    public function deleteUsersAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppCoreBundle:User')->find($request->get('userID'));

        if ($user !== null) {
            $em->remove($user);
            $em->flush();
        }
    }

    /**
     * @param Request $request
     * @param bool $add
     * @param bool $clearMissing
     * @return User|View|Form
     */
    private function handleUserData(Request $request, $add = true, $clearMissing = true)
    {
        $em = $this->getDoctrine()->getManager();

        if ($add) {
            $user = new User();
        } else {
            $user = $em->getRepository('AppCoreBundle:User')->findOneWithAddress($request->get('userID'));

            if ($user === null) {
                return View::create(array('message' => 'The user does not exists !'), Response::HTTP_NOT_FOUND);
            }
        }

        $form = $this->createForm(UserType::class, $user);
        $form->submit($request->request->all(), $clearMissing);

        if ($form->isSubmitted() && $form->isValid()) {

            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user, false);

            $em->flush();
            return $user;
        } else {
            return $form;
        }
    }

}