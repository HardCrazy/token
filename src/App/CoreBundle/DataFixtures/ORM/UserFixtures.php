<?php
/**
 * Created by PhpStorm.
 * User: Hard
 * Date: 05-07-17
 * Time: 23:56
 */

namespace App\CoreBundle\DataFixtures\ORM;


use App\CoreBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserFixtures extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $data = array(
            array('julien.coppin@bstorm.be', 'jucoppin', 'Test1234', true, true),
            array('student@yopmail.com', 'student', 'Test1234', true, false)
        );

        $userManager = $this->container->get('fos_user.user_manager');

        for ($i = 0; $i < count($data); $i++) {
            $user = new User();
            $user->setEmail($data[$i][0]);
            $user->setUsername($data[$i][1]);
            $user->setPlainPassword($data[$i][2]);
            $user->setEnabled($data[$i][3]);
            $user->setSuperAdmin($data[$i][4]);

            $userManager->updateUser($user);
            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}