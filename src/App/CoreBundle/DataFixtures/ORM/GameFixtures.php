<?php

/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 05-07-17
 * Time: 14:05
 */

namespace App\CoreBundle\DataFixtures\ORM;

use App\CoreBundle\Entity\Game;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class GameFixtures extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = \Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 25; $i++) {
            $game = new Game();
            $game->setGameName($faker->uuid);
            $game->setGameMinPlayer($faker->numberBetween(1, 15));
            if (rand(0, 1) == 0) {
                $game->setGameMaxPlayer($faker->numberBetween($game->getGameMinPlayer()));
            }

            $manager->persist($game);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}