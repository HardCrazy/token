<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 13-07-17
 * Time: 09:33
 */

namespace App\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class GamePart
 * @package App\CoreBundle\Entity
 *
 * @ORM\Table(name="GameParts")
 * @ORM\Entity()
 *
 * @UniqueEntity("gamePartName")
 */
class GamePart
{
    /**
     * @var integer
     *
     * @ORM\Column(name="GamePartID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $gamePartID;

    /**
     * @var string
     *
     * @ORM\Column(name="GamePartName", type="string", nullable=false, length=255, unique=true)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max="255"
     * )
     */
    private $gamePartName;

    /**
     * Get gamePartID
     *
     * @return integer
     */
    public function getGamePartID()
    {
        return $this->gamePartID;
    }

    /**
     * Set gamePartName
     *
     * @param string $gamePartName
     *
     * @return GamePart
     */
    public function setGamePartName($gamePartName)
    {
        $this->gamePartName = $gamePartName;

        return $this;
    }

    /**
     * Get gamePartName
     *
     * @return string
     */
    public function getGamePartName()
    {
        return $this->gamePartName;
    }
}
