<?php

/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 05-07-17
 * Time: 10:26
 */

namespace App\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 * @package App\SecurityBundle\Entity
 *
 * @ORM\Table(name="Users")
 * @ORM\Entity(repositoryClass="App\CoreBundle\Repository\UserRepository")
 *
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="UserID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Assert\Length(
     *     max="255"
     * )
     */
    protected $email;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min="5",
     *     max="255"
     * )
     */
    protected $username;

    /**
     * @var string
     *
     * @Assert\NotBlank(
     *     groups={"Update", "Create"}
     * )
     * @Assert\Length(
     *     min="6",
     *     max="255",
     *     groups={"Update", "Create"}
     * )
     *
     * @Serializer\Exclude()
     */
    protected $plainPassword;

    /**
     * @var string
     */
    protected $enabled;

    /**
     * @var \DateTime
     */
    protected $lastLogin;

    /**
     * @ORM\ManyToOne(targetEntity="App\CoreBundle\Entity\Address", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="AddressID", referencedColumnName="AddressID", nullable=true, onDelete="CASCADE")
     *
     * @Assert\Valid()
     */
    private $address;

    /**
     * Set address
     *
     * @param \App\CoreBundle\Entity\Address $address
     *
     * @return User
     */
    public function setAddress(\App\CoreBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \App\CoreBundle\Entity\Address
     */
    public function getAddress()
    {
        return $this->address;
    }
}
