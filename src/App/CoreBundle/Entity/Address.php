<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 10-07-17
 * Time: 12:11
 */

namespace App\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Address
 * @package App\CoreBundle\Entity
 *
 * @ORM\Table(name="Addresses")
 * @ORM\Entity
 */
class Address
{
    /**
     * @var integer
     *
     * @ORM\Column(name="AddressID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $addressID;

    /**
     * @var string
     *
     * @ORM\Column(name="AddressLine1", type="string", nullable=false, length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max="255"
     * )
     */
    private $addressLine1;

    /**
     * @var string
     *
     * @ORM\Column(name="AddressLine2", type="string", nullable=true, length=255)
     *
     * @Assert\Length(
     *     max="255"
     * )
     */
    private $addressLine2;

    /**
     * @var string
     *
     * @ORM\Column(name="AddressPostCode", type="string", nullable=false, length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max="255"
     * )
     */
    private $addressPostCode;

    /**
     * @var string
     *
     * @ORM\Column(name="AddressCity", type="string", nullable=false, length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max="255"
     * )
     */
    private $addressCity;

    /**
     * @var string
     *
     * @ORM\Column(name="AddressState", type="string", nullable=true, length=255)
     *
     * @Assert\Length(
     *     max="255"
     * )
     */
    private $addressState;

    /**
     * @var string
     *
     * @ORM\Column(name="AddressCountry", type="string", nullable=false, length=255)
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max="255"
     * )
     */
    private $addressCountry;

    /**
     * Get addressID
     *
     * @return integer
     */
    public function getAddressID()
    {
        return $this->addressID;
    }

    /**
     * Set addressLine1
     *
     * @param string $addressLine1
     *
     * @return Address
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;

        return $this;
    }

    /**
     * Get addressLine1
     *
     * @return string
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * Set addressLine2
     *
     * @param string $addressLine2
     *
     * @return Address
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;

        return $this;
    }

    /**
     * Get addressLine2
     *
     * @return string
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * Set addressPostCode
     *
     * @param string $addressPostCode
     *
     * @return Address
     */
    public function setAddressPostCode($addressPostCode)
    {
        $this->addressPostCode = $addressPostCode;

        return $this;
    }

    /**
     * Get addressPostCode
     *
     * @return string
     */
    public function getAddressPostCode()
    {
        return $this->addressPostCode;
    }

    /**
     * Set addressCity
     *
     * @param string $addressCity
     *
     * @return Address
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;

        return $this;
    }

    /**
     * Get addressCity
     *
     * @return string
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * Set addressState
     *
     * @param string $addressState
     *
     * @return Address
     */
    public function setAddressState($addressState)
    {
        $this->addressState = $addressState;

        return $this;
    }

    /**
     * Get addressState
     *
     * @return string
     */
    public function getAddressState()
    {
        return $this->addressState;
    }

    /**
     * Set addressCountry
     *
     * @param string $addressCountry
     *
     * @return Address
     */
    public function setAddressCountry($addressCountry)
    {
        $this->addressCountry = $addressCountry;

        return $this;
    }

    /**
     * Get addressCountry
     *
     * @return string
     */
    public function getAddressCountry()
    {
        return $this->addressCountry;
    }
}
