<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 06-07-17
 * Time: 09:55
 */

namespace App\CoreBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Credentials
{
    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max="255"
     * )
     */
    private $login;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     max="255"
     * )
     */
    private $password;

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }
}