<?php

/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 05-07-17
 * Time: 10:31
 */

namespace App\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Dotenv\Exception\PathException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Game
 * @package App\CoreBundle\Entity
 *
 * @ORM\Table(name="Games")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 *
 * @UniqueEntity("gameName")
 */
class Game
{
    /**
     * @var integer
     *
     * @ORM\Column(name="GameID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $gameID;

    /**
     * @var string
     *
     * @Assert\NotBlank()
     * @Assert\Length(
     *     min="2",
     *     max="255"
     * )
     *
     * @ORM\Column(name="GameName", type="string", nullable=false, length=255, unique=true)
     */
    private $gameName;

    /**
     * @var integer
     *
     * @Assert\NotBlank()
     * @Assert\GreaterThan(
     *     value="0"
     * )
     *
     * @ORM\Column(name="GameMinPlayer", type="integer", nullable=false)
     */
    private $gameMinPlayer;

    /**
     * @var integer
     *
     * @ORM\Column(name="GameMaxPlayer", type="integer", nullable=true)
     */
    private $gameMaxPlayer;

    /**
     * @var string
     *
     * @ORM\Column(name="GamePicture", type="string", length=255, nullable=true)
     */
    private $gamePicture;

    /**
     * @var UploadedFile
     *
     * @Assert\Image(
     *     maxSize="2M"
     * )
     *
     * @Serializer\Exclude()
     */
    private $file;

    /**
     * @ORM\ManyToMany(targetEntity="App\CoreBundle\Entity\GamePart",cascade={"persist"})
     * @ORM\JoinTable(name="GamesGameParts",
     *      joinColumns={@ORM\JoinColumn(name="GameID", referencedColumnName="GameID")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="GamePartID", referencedColumnName="GamePartID")}
     * )
     *
     * @Assert\Valid()
     */
    private $gameParts;

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateFile()
    {
        if ($this->file === null) {
            return;
        }

        $this->removeFile();

        $this->gamePicture = sprintf('%s.%s', uniqid(time() . '_', true), $this->file->getClientOriginalExtension());
        $path = $this->getPath();

        $this->file->move($path, $this->gamePicture);
        $this->file = null;
    }

    /**
     * @ORM\PreRemove()
     */
    public function removeFile()
    {
        if ($this->getGamePicture() === null) {
            return;
        }

        if (file_exists($this->getGamePicture())) {
            $path = $this->getPath();
            $filePath = join(DIRECTORY_SEPARATOR, array($path, $this->getGamePicture()));
            unlink($filePath);

            $this->gamePicture = null;
        }
    }

    /**
     * @return string
     */
    private function getPath()
    {
        $path = join(DIRECTORY_SEPARATOR, array(__DIR__, '..', '..', '..', '..', 'web', 'uploads'));
        if (realpath($path) === false) {
            throw new PathException($path);
        }
        return realpath($path);
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return bool
     *
     * @Assert\IsTrue(
     *     message="The max number of player must be greater or equal than the min number of player"
     * )
     */
    public function isPlayerNumberValid()
    {
        return ($this->gameMaxPlayer === null || $this->gameMaxPlayer >= $this->gameMinPlayer);
    }

    /**
     * Get gameID
     *
     * @return integer
     */
    public function getGameID()
    {
        return $this->gameID;
    }

    /**
     * Set gameName
     *
     * @param string $gameName
     *
     * @return Game
     */
    public function setGameName($gameName)
    {
        $this->gameName = $gameName;

        return $this;
    }

    /**
     * Get gameName
     *
     * @return string
     */
    public function getGameName()
    {
        return $this->gameName;
    }

    /**
     * Set gameMinPlayer
     *
     * @param integer $gameMinPlayer
     *
     * @return Game
     */
    public function setGameMinPlayer($gameMinPlayer)
    {
        $this->gameMinPlayer = $gameMinPlayer;

        return $this;
    }

    /**
     * Get gameMinPlayer
     *
     * @return integer
     */
    public function getGameMinPlayer()
    {
        return $this->gameMinPlayer;
    }

    /**
     * Set gameMaxPlayer
     *
     * @param integer $gameMaxPlayer
     *
     * @return Game
     */
    public function setGameMaxPlayer($gameMaxPlayer)
    {
        $this->gameMaxPlayer = $gameMaxPlayer;

        return $this;
    }

    /**
     * Get gameMaxPlayer
     *
     * @return integer
     */
    public function getGameMaxPlayer()
    {
        return $this->gameMaxPlayer;
    }

    /**
     * Set gamePicture
     *
     * @param string $gamePicture
     *
     * @return Game
     */
    public function setGamePicture($gamePicture)
    {
        $this->gamePicture = $gamePicture;

        return $this;
    }

    /**
     * Get gamePicture
     *
     * @return string
     */
    public function getGamePicture()
    {
        return $this->gamePicture;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->gameParts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add gamePart
     *
     * @param \App\CoreBundle\Entity\GamePart $gamePart
     *
     * @return Game
     */
    public function addGamePart(\App\CoreBundle\Entity\GamePart $gamePart)
    {
        $this->gameParts[] = $gamePart;

        return $this;
    }

    /**
     * Remove gamePart
     *
     * @param \App\CoreBundle\Entity\GamePart $gamePart
     */
    public function removeGamePart(\App\CoreBundle\Entity\GamePart $gamePart)
    {
        $this->gameParts->removeElement($gamePart);
    }

    /**
     * Get gameParts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGameParts()
    {
        return $this->gameParts;
    }
}
