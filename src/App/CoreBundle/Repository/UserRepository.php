<?php

/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 07-07-17
 * Time: 09:37
 */
namespace App\CoreBundle\Repository;

use App\CoreBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * @return User[]
     */
    public function findAllWithAddress()
    {
        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.address', 'a')
            ->addSelect('a');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $userID
     * @return User
     */
    public function findOneWithAddress($userID)
    {
        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.address', 'a')
            ->addSelect('a')
            ->where('u.id = :userID')
            ->setParameter('userID', $userID);

        return $qb->getQuery()->getOneOrNullResult();
    }
}