<?php
/**
 * Created by PhpStorm.
 * User: Hard
 * Date: 06-07-17
 * Time: 00:08
 */

namespace App\CoreBundle\Form;


use App\CoreBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class);
        $builder->add('email', EmailType::class);
        $builder->add('plainPassword', PasswordType::class);
        $builder->add('enabled', CheckboxType::class);
        $builder->add('address', AddressType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', 'App\\CoreBundle\\Entity\\User');

        $resolver->setDefault('validation_groups', function(FormInterface $form) {
            /** @var User $data */
            $data = $form->getData();

            $roles = array('Default');

            if ($data->getId() === null) {
                $roles[] = 'Create';
            } else if ($data->getPlainPassword() !== null) {
                $roles[] = 'Update';
            }

            return $roles;
        });
    }
}