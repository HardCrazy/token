<?php
/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 10-07-17
 * Time: 12:16
 */

namespace App\CoreBundle\Form;


use App\CoreBundle\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('addressLine1', TextType::class);
        $builder->add('addressLine2', TextType::class);
        $builder->add('addressPostCode', TextType::class);
        $builder->add('addressCity', TextType::class);
        $builder->add('addressState', TextType::class);
        $builder->add('addressCountry', TextType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Address::class);
    }
}