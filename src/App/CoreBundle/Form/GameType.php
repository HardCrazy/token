<?php

/**
 * Created by PhpStorm.
 * User: Julien
 * Date: 05-07-17
 * Time: 16:18
 */

namespace App\CoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('gameName', TextType::class);
        $builder->add('gameMinPlayer', IntegerType::class);
        $builder->add('gameMaxPlayer', IntegerType::class);
        $builder->add('file', FileType::class);
        $builder->add('gameParts', CollectionType::class, array(
            'entry_type' => GamePartType::class,
            'allow_add' => true
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', 'App\\CoreBundle\\Entity\\Game');
    }
}